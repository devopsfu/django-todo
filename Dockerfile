FROM python
WORKDIR /usr/src/app
COPY ./todoApp .
RUN pip install -r requirements.txt
ENV POSTGRES_NAME=todo
ENV POSTGRES_USER=postgres
ENV POSTGRES_PASSWORD=password
ENV POSTGRES_HOST=localhost
ENV POSTGRES_PORT=5432
ENV ELASTIC_SERVICE_NAME=elasticsearch
# ENV ELASTIC_SECRET_TOKEN=5432

CMD [ "python", "./manage.py", "runserver", "0.0.0.0:8000" ]